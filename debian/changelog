r-bioc-sva (3.54.0-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Mon, 13 Jan 2025 21:36:42 +0100

r-bioc-sva (3.54.0-1) experimental; urgency=medium

  * Team upload.
  * New upstream version
  * Standards-Version: 4.7.0 (routine-update)
  * dh-update-R to update Build-Depends (routine-update)

 -- Michael R. Crusoe <crusoe@debian.org>  Fri, 08 Nov 2024 10:58:39 +0100

r-bioc-sva (3.52.0-1) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.

 -- Michael R. Crusoe <crusoe@debian.org>  Sun, 18 Aug 2024 12:18:31 +0200

r-bioc-sva (3.52.0-1~0exp1) experimental; urgency=medium

  * Team upload.
  * d/{,tests/}control: bump minimum versions of r-bioc-* packages to
    bioc-3.19+ versions

 -- Michael R. Crusoe <crusoe@debian.org>  Tue, 23 Jul 2024 15:42:11 +0200

r-bioc-sva (3.52.0-1~0exp) experimental; urgency=low

  * Team upload
  * New upstream version
  * Set upstream metadata fields: Archive.
  * d/control: Skip building on 32-bit systems.

 -- Michael R. Crusoe <crusoe@debian.org>  Sat, 13 Jul 2024 14:20:06 +0200

r-bioc-sva (3.50.0-1) unstable; urgency=medium

  * New upstream version

 -- Andreas Tille <tille@debian.org>  Fri, 01 Dec 2023 06:58:08 +0100

r-bioc-sva (3.48.0-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.2 (routine-update)

 -- Andreas Tille <tille@debian.org>  Thu, 27 Jul 2023 15:19:31 +0200

r-bioc-sva (3.46.0-1) unstable; urgency=medium

  * New upstream version
  * Reduce piuparts noise in transitions (routine-update)
  * Fix lintian-overrides syntax

 -- Andreas Tille <tille@debian.org>  Mon, 21 Nov 2022 22:08:18 +0100

r-bioc-sva (3.44.0-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.1 (routine-update)
  * Reorder sequence of d/control fields by cme (routine-update)

 -- Andreas Tille <tille@debian.org>  Mon, 16 May 2022 21:24:35 +0200

r-bioc-sva (3.42.0-1) unstable; urgency=medium

  * Disable reprotest
  * New upstream version

 -- Andreas Tille <tille@debian.org>  Fri, 26 Nov 2021 08:18:45 +0100

r-bioc-sva (3.40.0-1) unstable; urgency=medium

  * New upstream version
  * Standards-Version: 4.6.0 (routine-update)
  * dh-update-R to update Build-Depends (3) (routine-update)
  * Add lintian-override
  * Provide autopkgtest-pkg-r.conf to make sure Test-Depends will be found
  * Drop debian/tests/control and rely on autopkgtest-pkg-r

 -- Andreas Tille <tille@debian.org>  Wed, 08 Sep 2021 10:51:49 +0200

r-bioc-sva (3.38.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version
  * debhelper-compat 13 (routine-update)

 -- Dylan Aïssi <daissi@debian.org>  Tue, 03 Nov 2020 12:23:28 +0100

r-bioc-sva (3.36.0-1) unstable; urgency=medium

  * Team upload.
  * New upstream version

 -- Dylan Aïssi <daissi@debian.org>  Wed, 20 May 2020 17:20:03 +0200

r-bioc-sva (3.34.0-2) unstable; urgency=medium

  * Trim trailing whitespace.

 -- Andreas Tille <tille@debian.org>  Mon, 06 Apr 2020 15:14:14 +0200

r-bioc-sva (3.34.0-1) unstable; urgency=medium

  * Initial release (closes: #955739)

 -- Andreas Tille <tille@debian.org>  Sat, 04 Apr 2020 14:32:57 +0200
